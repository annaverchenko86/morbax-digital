1. IE compatibility mode

	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">


2. Attribute order

	class
	id
	name
	data-*
	src
	for
	type
	href
	value
	title
	alt
	role
	aria-*
	boolean attributes (checked, disabled, etc.)


3. Reducing markup

	<!-- Not so great -->
	<span class="avatar">
  		<img src="...">
	</span>

	<!-- Better -->
	<img class="avatar" src="...">


4. Don't use @import

	Compared to <link>s, @import is slower, adds extra page requests, and can cause other unforeseen problems.


5. Media query placement

	Place media queries as close to their relevant rule sets whenever possible. Don't bundle them all in a separate stylesheet or at the end of the document.


6. Prefixed properties

	Prefixed properties:

	selector {
		-webkit-box-shadow: inset 0 1px 5px rgba(0, 0, 0, 0.2);
		   -moz-box-shadow: inset 0 1px 5px rgba(0, 0, 0, 0.2);
		    -ms-box-shadow: inset 0 1px 5px rgba(0, 0, 0, 0.2);
		     -o-box-shadow: inset 0 1px 5px rgba(0, 0, 0, 0.2);
		        box-shadow: inset 0 1px 5px rgba(0, 0, 0, 0.2);
	}